package mobilehm.model;

public class EmailMessage {
    private String destinationEmail;
    private String subject;
    private String text;

    public EmailMessage(String destinationEmail, String subject, String text) {
        this.destinationEmail = destinationEmail;
        this.subject = subject;
        this.text = text;
    }

    public String getDestinationEmail() {
        return destinationEmail;
    }

    public String getSubject() {
        return subject;
    }

    public String getText() {
        return text;
    }

}
