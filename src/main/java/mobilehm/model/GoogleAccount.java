package mobilehm.model;

public class GoogleAccount {
    private String email;
    private String password;

    public GoogleAccount(){}

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
