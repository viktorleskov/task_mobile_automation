package mobilehm.utils.csv;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class CSVFileReader {
    static List<String> readCSVDataSetByTokens(String fileLocation) {
        List<String> linesList = new ArrayList<>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(fileLocation));
            String[] wordsAtLine;
            while ((wordsAtLine = reader.readNext()) != null) {
                linesList.addAll(Arrays.asList(wordsAtLine));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return linesList;
    }
}
