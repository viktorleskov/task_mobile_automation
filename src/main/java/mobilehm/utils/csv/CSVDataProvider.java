package mobilehm.utils.csv;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public abstract class CSVDataProvider {
    public static <T> List<T> getModelsFromFile(Class<T> modelToBeProvided, String fileName) {
        List<String> stringDataSet = CSVFileReader.readCSVDataSetByTokens(fileName);
        List<T> objectsFromFile = new ArrayList<>();
        Field[] fields = modelToBeProvided.getDeclaredFields();
        setAccessForAllFields(fields);
        T newInstance = null;
        for (int currentDataPointer = 0; currentDataPointer < stringDataSet.size(); ) {
            try {
                newInstance = modelToBeProvided.newInstance();
                for (Field field : fields) {
                    field.set(newInstance, stringDataSet.get(currentDataPointer));
                    currentDataPointer++;
                }
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            objectsFromFile.add(newInstance);
        }
        return objectsFromFile;
    }

    private static void setAccessForAllFields(Field[] fields) {
        for (Field field : fields) {
            field.setAccessible(true);
        }
    }
}
