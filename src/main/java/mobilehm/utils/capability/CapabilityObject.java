package mobilehm.utils.capability;

public class CapabilityObject {
    private String capabilityName;
    private Object value;

    public CapabilityObject() {
    }

    String getCapabilityName() {
        return capabilityName;
    }

    Object getValue() {
        return value;
    }
}
