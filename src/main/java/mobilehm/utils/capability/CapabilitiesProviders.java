package mobilehm.utils.capability;

import org.openqa.selenium.remote.DesiredCapabilities;
import mobilehm.utils.csv.CSVDataProvider;

import java.util.List;

public abstract class CapabilitiesProviders {
    public static DesiredCapabilities getCapabilitiesFromFile(String fileName) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        List<CapabilityObject> singleCapabilities = CSVDataProvider.getModelsFromFile(CapabilityObject.class, fileName);
        singleCapabilities.forEach(capability -> capabilities.setCapability(
                capability.getCapabilityName(),
                capability.getValue()));
        return capabilities;
    }
}
