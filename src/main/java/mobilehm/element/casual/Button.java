package mobilehm.element.casual;

import mobilehm.element.PageElement;
import org.openqa.selenium.WebElement;

public class Button extends PageElement {
    public Button(WebElement mobileElement) {
        super(mobileElement);
    }

    @Override
    public void click() {
        webElement.click();
    }
}
