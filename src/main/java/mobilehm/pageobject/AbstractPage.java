package mobilehm.pageobject;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import mobilehm.driver.DriverClient;
import mobilehm.element.CustomDecorator;

public abstract class AbstractPage {
    public AbstractPage() {
        PageFactory.initElements(new CustomDecorator(new DefaultElementLocatorFactory(DriverClient.getDriver())), this);
    }
}
