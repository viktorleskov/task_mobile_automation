package mobilehm.pageobject.gmail;

import mobilehm.element.casual.Button;
import mobilehm.pageobject.AbstractPage;
import mobilehm.utils.CustomWaiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class DraftsPage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(DraftsPage.class);

    public DraftsPage() {
    }

    @FindBy(xpath = "//android.view.View[not(@content-desc)]")
    private Button firstDraftsMessage;

    @FindBy(xpath = "//android.widget.ImageView[@content-desc='More options']")
    private Button moreOptionsButton;

    //sorry for absolute path_)
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]/android.widget.LinearLayout")
    private Button moveToInboxButton;

    @FindBy(id = "edit_draft")
    private Button editDraftButton;

    public DraftsPage clickFirstDraftMessage(){
        CustomWaiters.waitForElementToBeClickable(firstDraftsMessage).click();
        LOG.info("firstDraftsMessage button was clicked.");
        return this;
    }

    public DraftsPage clickMoreOptionsButton(){
        CustomWaiters.waitForElementToBeClickable(moreOptionsButton).click();
        LOG.info("MoreOptionsButton button was clicked.");
        return this;
    }

    public DraftsPage clickMoveToInboxButton(){
        CustomWaiters.waitForElementToBeClickable(moveToInboxButton).click();
        LOG.info("moveToInboxButton button was clicked.");
        return this;
    }

    public DraftsPage clickEditDraftButton(){
        CustomWaiters.waitForElementToBeClickable(editDraftButton).click();
        LOG.info("editDraftButton button was clicked.");
        return this;
    }
}
