package mobilehm.pageobject.gmail;

import mobilehm.element.casual.Button;
import mobilehm.pageobject.AbstractPage;
import mobilehm.pageobject.gmail.businessobject.NavigationMenuBO;
import mobilehm.utils.CustomWaiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class GmailHomePage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(GmailHomePage.class);

    public GmailHomePage() {
    }

    @FindBy(id = "compose_button")
    private Button composeButton;

    @FindBy(xpath = "//*[@content-desc='Open navigation drawer']")
    private Button navigationMenuButton;


    public GmailHomePage clickComposeButton() {
        CustomWaiters.waitForElementToBeClickable(composeButton).click();
        LOG.info("compose button was clicked.");
        return this;
    }

    public NavigationMenuBO clickNavigationMenuButton() {
        CustomWaiters.waitForElementToBeClickable(navigationMenuButton).click();
        LOG.info("navigationMenuButton button was clicked.");
        return new NavigationMenuBO();
    }

}
