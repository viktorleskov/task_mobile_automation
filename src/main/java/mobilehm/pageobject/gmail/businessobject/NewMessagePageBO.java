package mobilehm.pageobject.gmail.businessobject;

import mobilehm.model.EmailMessage;
import mobilehm.pageobject.gmail.NewMessagePage;

public class NewMessagePageBO {
    private NewMessagePage newMessagePage;

    public NewMessagePageBO() {
        newMessagePage = new NewMessagePage();
    }

    public NewMessagePageBO prepareMessage(EmailMessage message) {
        newMessagePage
                .enterDestinationEmail(message.getDestinationEmail())
                .enterSubject(message.getSubject());
        //.enterMessageText(message.getText());
        return this;
    }

    public GmailHomePageBO clickSendButton() {
        newMessagePage.clickSendMessageButton();
        return new GmailHomePageBO();
    }

    public GmailHomePageBO clickBackButton() {
        newMessagePage.clickBackButton();
        return new GmailHomePageBO();
    }


}
