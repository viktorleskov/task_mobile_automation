package mobilehm.pageobject.gmail.businessobject;

import mobilehm.pageobject.gmail.DraftsPage;

public class DraftsPageBO {
    private DraftsPage draftsPage;

    public DraftsPageBO(){
        draftsPage = new DraftsPage();
    }

    public NewMessagePageBO editFirstDraftMessage(){
        draftsPage
                .clickFirstDraftMessage()
                .clickMoreOptionsButton()
                .clickMoveToInboxButton()
                .clickEditDraftButton();
        return new NewMessagePageBO();
    }
}
