package mobilehm.pageobject.gmail.businessobject;

import mobilehm.pageobject.gmail.GmailLoginPage;

public class GmailLoginBO {
    private GmailLoginPage gmailLoginPage;

    public GmailLoginBO() {
        gmailLoginPage = new GmailLoginPage();
    }

    public GmailLoginBO addAnotherGoogleAccount(String email, String password) {
        gmailLoginPage
                .clickAddAnotherEmailButton()
                .clickGoogleAccountButton()
                .inputEmail(email)
                .clickEmailNextButton()
                .inputPassword(password)
                .clickPasswordNextButton()
                .clickAgreeButtonButton()
                .skipServicesSetting();
        return this;
    }

    public GmailHomePageBO proceedToGmail() {
        gmailLoginPage.clickProceedToGmailButton();
        return new GmailHomePageBO();
    }
}
