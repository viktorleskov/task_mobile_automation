package mobilehm.pageobject.gmail.businessobject;

import mobilehm.pageobject.gmail.GmailHomePage;

public class GmailHomePageBO {
    private GmailHomePage gmailHomePage;

    public GmailHomePageBO() {
        gmailHomePage = new GmailHomePage();
    }

    public NewMessagePageBO clickComposeButton(){
        gmailHomePage.clickComposeButton();
        return new NewMessagePageBO();
    }

    public NavigationMenuBO openNavigationMenu(){
        gmailHomePage.clickNavigationMenuButton();
        return new NavigationMenuBO();
    }

}
