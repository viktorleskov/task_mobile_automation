package mobilehm.pageobject.gmail.businessobject;

import mobilehm.pageobject.gmail.NavigationMenuPage;

public class NavigationMenuBO {
    private NavigationMenuPage navigationMenuPage;

    public NavigationMenuBO() {
        navigationMenuPage = new NavigationMenuPage();
    }

    public DraftsPageBO proceedToDrafts() {
        navigationMenuPage.clickDraftsButton();
        return new DraftsPageBO();
    }
}
