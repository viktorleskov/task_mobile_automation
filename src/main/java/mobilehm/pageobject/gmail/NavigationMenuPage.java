package mobilehm.pageobject.gmail;

import mobilehm.element.casual.Button;
import mobilehm.pageobject.AbstractPage;
import mobilehm.utils.CustomWaiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class NavigationMenuPage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(NavigationMenuPage.class);

    public NavigationMenuPage() {
    }

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[1]")
    public Button primaryButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[2]")
    public Button socialButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[3]")
    public Button promotionsButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[4]")
    public Button starredButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[5]")
    public Button snoozedButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[6]")
    public Button importantButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[7]")
    public Button sentButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[8]")
    public Button outBoxButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[9]")
    public Button draftsButton;

    @FindBy(xpath = "(//*[@resource-id='com.google.android.gm:id/folder_content_wrapper'])[10]")
    public Button allMailButton;

    public void clickPrimaryButton() {
        CustomWaiters.waitForVisibilityOfElement(primaryButton).click();
        LOG.info("primaryButton button was clicked");
    }

    public void clickSocialButton() {
        CustomWaiters.waitForVisibilityOfElement(socialButton).click();
        LOG.info("socialButton button was clicked");
    }

    public void clickPromotionsButton() {
        CustomWaiters.waitForVisibilityOfElement(promotionsButton).click();
        LOG.info("promotionsButton button was clicked");
    }

    public void clickStarredButton() {
        CustomWaiters.waitForVisibilityOfElement(starredButton).click();
        LOG.info("starredButton button was clicked");
    }

    public void clickSnoozedButton() {
        CustomWaiters.waitForVisibilityOfElement(snoozedButton).click();
        LOG.info("snoozedButton button was clicked");
    }

    public void clickImportantButton() {
        CustomWaiters.waitForVisibilityOfElement(importantButton).click();
        LOG.info("importantButton button was clicked");
    }

    public void clickSentButton() {
        CustomWaiters.waitForVisibilityOfElement(sentButton).click();
        LOG.info("sentButton button was clicked");
    }

    public void clickOutBoxButton() {
        CustomWaiters.waitForVisibilityOfElement(outBoxButton).click();
        LOG.info("outBoxButton button was clicked");
    }

    public void clickDraftsButton() {
        CustomWaiters.waitForVisibilityOfElement(draftsButton).click();
        LOG.info("draftsButton button was clicked");
    }

    public void clickAllMailButton() {
        CustomWaiters.waitForVisibilityOfElement(allMailButton).click();
        LOG.info("allMailButton button was clicked");
    }
}
