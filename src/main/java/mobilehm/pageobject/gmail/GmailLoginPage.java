package mobilehm.pageobject.gmail;

import mobilehm.element.casual.Button;
import mobilehm.element.casual.TextInput;
import mobilehm.pageobject.AbstractPage;
import mobilehm.utils.CustomWaiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(GmailLoginPage.class);

    public GmailLoginPage() {
    }

    @FindBy(id = "setup_addresses_add_another")
    private Button addAnotherEmailButton;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.google.android.gm:id/account_setup_item']")
    private Button googleAccountButton;

    @FindBy(id = "action_done")
    private Button proceedToGmailButton;

    @FindBy(id = "owner")
    private Button addedAccount;

    @FindBy(xpath = "//android.widget.EditText[@resource-id='identifierId']")
    private TextInput emailInputField;

    @FindBy(xpath = "//*[@resource-id='identifierNext']")
    private Button emailNextButton;

    @FindBy(xpath = "//*[@resource-id='passwordNext']")
    private Button passwordNextButton;

    @FindBy(xpath = "//*[@resource-id='password']//android.widget.EditText")
    private TextInput passwordTextInput;

    @FindBy(xpath = "//*[@resource-id='signinconsentNext']")
    private Button agreeButton;

    @FindBy(id = "com.google.android.gms:id/sud_navbar_next")
    private Button acceptButton;

    @FindBy(xpath = "//*[@resource-id='com.google.android.gms:id/sud_navbar_more']")
    private Button servicesMoreButton;

    @FindBy(xpath = "//*[@resource-id='com.google.android.gms:id/sud_navbar_next']")
    private Button servicesNextButton;

    public GmailLoginPage clickAddAnotherEmailButton() {
        CustomWaiters.waitForElementToBeClickable(addAnotherEmailButton).click();
        LOG.info("addAnotherEmail button was successfully clicked.");
        return this;
    }

    public GmailLoginPage clickGoogleAccountButton() {
        CustomWaiters.waitForElementToBeClickable(googleAccountButton).click();
        LOG.info("GoogleAccount button was successfully clicked.");
        return this;
    }

    public GmailLoginPage clickProceedToGmailButton() {
        CustomWaiters.waitForVisibilityOfElement(addedAccount);
        CustomWaiters.waitForElementToBeClickable(proceedToGmailButton).click();
        LOG.info("proceedToGmail button was successfully clicked.");
        return this;
    }

    public GmailLoginPage inputEmail(String email) {
        CustomWaiters.waitForElementToBeClickable(emailInputField).click();
        LOG.info("email input field was successfully selected.");
        CustomWaiters.waitForVisibilityOfElement(emailInputField).sendKeys(email);
        LOG.info("email was successfully entered.");
        return this;
    }

    public GmailLoginPage clickEmailNextButton() {
        CustomWaiters.waitForVisibilityOfElement(emailNextButton).click();
        LOG.info("emailNext button was successfully clicked.");
        return this;
    }

    public GmailLoginPage clickPasswordNextButton() {
        CustomWaiters.waitForElementToBeClickable(passwordNextButton).click();
        LOG.info("passwordNext button was successfully clicked.");
        return this;
    }

    public GmailLoginPage inputPassword(String password) {
        CustomWaiters.waitForElementToBeClickable(passwordTextInput).click();
        LOG.info("password input field was successfully selected.");
        CustomWaiters.waitForVisibilityOfElement(passwordTextInput).sendKeys(password);
        LOG.info("password was successfully entered.");
        return this;
    }

    public GmailLoginPage clickAgreeButtonButton() {
        CustomWaiters.waitForElementToBeClickable(agreeButton).click();
        LOG.info("agreeButton button was successfully clicked.");
        return this;
    }

    public GmailLoginPage clickAcceptButton() {
        CustomWaiters.waitForElementToBeClickable(acceptButton).click();
        LOG.info("accept button was successfully clicked.");
        return this;
    }

    public GmailLoginPage skipServicesSetting() {
        CustomWaiters.waitForElementToBeClickable(servicesMoreButton).click();
        LOG.info("servicesMoreButton button was successfully clicked.");
        CustomWaiters.waitForElementToBeClickable(servicesNextButton).click();
        LOG.info("servicesNextButton button was successfully clicked.");
        return this;
    }

}
