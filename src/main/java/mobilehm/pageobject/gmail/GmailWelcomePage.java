package mobilehm.pageobject.gmail;

import mobilehm.element.casual.Button;
import mobilehm.pageobject.AbstractPage;
import mobilehm.pageobject.gmail.businessobject.GmailLoginBO;
import mobilehm.utils.CustomWaiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class GmailWelcomePage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(GmailWelcomePage.class);

    @FindBy(id = "welcome_tour_got_it")
    private Button gotItButton;

    public GmailLoginBO skipWelcomePage() {
        LOG.info("Trying to click gotIt button");
        CustomWaiters.waitForVisibilityOfElement(gotItButton).click();
        LOG.info("gotIt button successfully clicked");
        return new GmailLoginBO();
    }
}
