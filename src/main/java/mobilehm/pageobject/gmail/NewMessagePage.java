package mobilehm.pageobject.gmail;

import mobilehm.element.casual.Button;
import mobilehm.element.casual.TextInput;
import mobilehm.pageobject.AbstractPage;
import mobilehm.utils.CustomWaiters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class NewMessagePage extends AbstractPage {
    private static Logger LOG = LogManager.getLogger(NewMessagePage.class);

    public NewMessagePage() {
    }

    @FindBy(id = "send")
    private Button sendButton;

    @FindBy(xpath = "//*[@content-desc='Navigate up']")
    private Button backButton;

    @FindBy(id = "to")
    private TextInput destinationEmailField;

    @FindBy(id = "subject")
    private TextInput subjectField;

    @FindBy(id = "//android.webkit.WebView/android.view.View")
    private TextInput messageTextField;

    public NewMessagePage clickSendMessageButton() {
        CustomWaiters.waitForElementToBeClickable(sendButton).click();
        LOG.info("send button was clicked");
        return this;
    }

    public NewMessagePage clickBackButton() {
        CustomWaiters.waitForElementToBeClickable(backButton).click();
        LOG.info("backButton button was clicked");
        return this;
    }

    public NewMessagePage enterDestinationEmail(String destinationEmail) {
        CustomWaiters.waitForVisibilityOfElement(destinationEmailField).sendKeys(destinationEmail + "\n");
        LOG.info(" email " + destinationEmail + " was entered");
        return this;
    }

    public NewMessagePage enterSubject(String subject) {
        CustomWaiters.waitForVisibilityOfElement(subjectField).sendKeys(subject);
        LOG.info("subject " + subject + " was entered");
        return this;
    }

    public NewMessagePage enterMessageText(String message) {
        CustomWaiters.waitForVisibilityOfElement(messageTextField).sendKeys(message);
        LOG.info("message was entered.");
        return this;
    }

}
