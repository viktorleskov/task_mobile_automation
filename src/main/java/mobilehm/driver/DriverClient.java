package mobilehm.driver;

import io.appium.java_client.android.AndroidDriver;
import mobilehm.utils.ConfigReaders;
import mobilehm.utils.capability.CapabilitiesProviders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public abstract class DriverClient {
    private static Logger LOG = LogManager.getLogger(DriverClient.class);
    private static String currentCapabilitiesFile = ConfigReaders.read("basic_capability");
    private static AndroidDriver androidDriver;

    public static AndroidDriver getDriver() {
        return Optional.ofNullable(androidDriver).isPresent() ?
                /*if present*/      androidDriver :
                /*if NOT present*/  initializeDriver();
    }

    private static AndroidDriver initializeDriver() {
        try {
            androidDriver = new AndroidDriver(new URL(ConfigReaders.read("APPIUM_URL") + "/wd/hub"),
                    CapabilitiesProviders.getCapabilitiesFromFile(currentCapabilitiesFile));
        } catch (MalformedURLException e) {
            LOG.info("Illegal URL was entered.");
            e.printStackTrace();
        }
        androidDriver.manage().timeouts().
                implicitlyWait(Integer.parseInt(ConfigReaders.read("basic_implicit_timeout")), TimeUnit.SECONDS);
        return androidDriver;
    }

    public static void quitDriver() {
        Optional.ofNullable(androidDriver).ifPresent(thenExecute -> {
            androidDriver.closeApp();
            LOG.debug("Application was closed successfully");
            androidDriver.quit();
            androidDriver = null;
            LOG.debug("Driver was closed!");
        });
    }

    public static void setCurrentCapabilitiesFile(String currentCapabilitiesFile) {
        DriverClient.currentCapabilitiesFile = currentCapabilitiesFile;
    }

    public static String getTextBy(By by){
        return androidDriver.findElement(by).getText();
    }

}
