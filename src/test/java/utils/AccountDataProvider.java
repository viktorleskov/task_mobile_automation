package utils;

import mobilehm.model.GoogleAccount;
import mobilehm.utils.csv.CSVDataProvider;
import org.testng.annotations.DataProvider;

import java.util.Iterator;
import java.util.List;

public class AccountDataProvider {
    @DataProvider(parallel = false)
    private Iterator<Object> userList() {
        List objects = CSVDataProvider.getModelsFromFile(GoogleAccount.class, "src/test/resources/AccountData.csv");
        return objects.iterator();
    }
}
