package appium.gmail;

import appium.BaseTest;
import mobilehm.driver.DriverClient;
import mobilehm.model.EmailMessage;
import mobilehm.model.GoogleAccount;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.AccountDataProvider;

public class GmailTest extends BaseTest {
    @BeforeClass
    static void initAll() {
        LOG = LogManager.getLogger(GmailTest.class);
        LOG.info("Test class running");
    }

    @Test(dataProvider = "userList", dataProviderClass = AccountDataProvider.class)
    public void saveMessageAsDraftThenSendTest(GoogleAccount currentTestAccount) {
        String subject = "test";
        start()
                .skipWelcomePage()
                .addAnotherGoogleAccount(currentTestAccount.getEmail(), currentTestAccount.getPassword())
                .proceedToGmail()
                .clickComposeButton()
                .prepareMessage(new EmailMessage(currentTestAccount.getEmail(), subject, ""))
                .clickBackButton()
                .openNavigationMenu()
                .proceedToDrafts()
                .editFirstDraftMessage()
                .clickSendButton();
        Assert.assertTrue(DriverClient.getTextBy(By.id("subject_and_folder_view")).contains(subject));
    }

    @AfterClass
    static void tearDownAll() {
        LOG.info("Finished.");
    }
}
