package appium;

import io.appium.java_client.android.AndroidDriver;
import mobilehm.driver.DriverClient;
import mobilehm.pageobject.gmail.GmailWelcomePage;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    protected static Logger LOG;
    protected static AndroidDriver driver;
    private GmailWelcomePage start;

    @BeforeMethod
    protected void init() {
        LOG.info("Test running.");
        start = new GmailWelcomePage();
        driver = DriverClient.getDriver();
    }

    @AfterMethod
    protected void tearDown() {
        LOG.info("Test done.");
        DriverClient.quitDriver();
    }

    protected GmailWelcomePage start() {
        return start;
    }

}
